package pl.lukaszporowski.pb01_04_fragments;

import pl.lukaszporowski.pb01_04_fragments.PanelFragmentImpl.PanelFragmentListener;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.app.Activity;
import android.graphics.Color;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends FragmentActivity implements PanelFragmentListener{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		FragmentManager fm = getSupportFragmentManager();
		
		if (savedInstanceState == null)
		{
			Fragment panelFragment = PanelFragmentImpl.newInstance(Color.GREEN, Color.RED, Color.BLUE);
			fm.beginTransaction().add(R.id.LiTop, panelFragment).commit();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		int itemId = item.getItemId();
		if (itemId == R.id.action_settings)
		{
			onGadajDoFragmentu();
			return true;
		}
		else
		{
			return super.onOptionsItemSelected(item);
		}
	}
	
	private void onGadajDoFragmentu()
	{
		FragmentManager fm = getSupportFragmentManager();
		PanelFragment panelFragment = (PanelFragment) fm.findFragmentById(R.id.LiTop);
		panelFragment.onGadajDoFragmentu();
	}

	@Override
	public void onColorClick(int blue) {
		Toast.makeText(getApplicationContext(), "kliknięty kolor", Toast.LENGTH_LONG).show();
		// TODO Auto-generated method stub
		
	}
	
		

}
