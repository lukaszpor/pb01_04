package pl.lukaszporowski.pb01_04_fragments;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

public class PanelFragmentImpl extends Fragment implements PanelFragment{
	
	private static final String COLORS_ARG = "colors arg";
	public interface PanelFragmentListener{

		void onColorClick(int blue);}

	private PanelFragmentListener listener;
	
	@Override
	public void onAttach(Activity activity)
	{
		super.onAttach(activity);
		listener = (PanelFragmentListener) activity;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		ViewGroup view = (ViewGroup) inflater.inflate(R.layout.panel_fragment, null);
		Bundle arguments = getArguments();
		int[] colors = arguments.getIntArray(COLORS_ARG);
		for (int i: colors)
		{
			
		}
		ImageView btn = new ImageView(getActivity());
		Drawable drawable = new ColorDrawable(Color.BLUE);
		btn.setImageDrawable(drawable);
		btn.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				listener.onColorClick(Color.BLUE);
				
			}
			
		});
		view.addView(btn, 50, 50);
		return view;
	}

	public static Fragment newInstance(int ...colors) {
		
		Fragment fragment = new PanelFragmentImpl();
		Bundle bundle = new Bundle();
		bundle.putIntArray(COLORS_ARG, colors);
		fragment.setArguments(bundle);
		return fragment;
	}
	

	@Override
	public void onGadajDoFragmentu() {
		Toast.makeText(getActivity(), "Gadaj do fragmentu wywołane", Toast.LENGTH_LONG).show();
		// TODO Auto-generated method stub
		
	}
	
}
